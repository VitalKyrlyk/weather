package testapp.weather.RealmDB;

import io.realm.RealmObject;

public class WeatherDB extends RealmObject{

    private long id;
    private long sortedId;
    private String city;
    private String temp;
    private String image;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getSortedId() {
        return sortedId;
    }

    public void setSortedId(long sortedId) {
        this.sortedId = sortedId;
    }
}
