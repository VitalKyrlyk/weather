package testapp.weather;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmResults;
import testapp.weather.RealmDB.LocationDB;


public class Map extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    GoogleMap googleMap;
    Button buttonOk, buttonCancel;
    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
    LatLng markerPosition;
    LatLng myLocation;
    Marker marker;
    Realm realm;

    boolean markerAdd = false;
    boolean markerLocation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        buttonOk = (Button) findViewById(R.id.buttonOk);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonOk.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        int error = 1;

        switch (v.getId()) {

            case R.id.buttonOk:

                if (markerAdd == true) {
                    realm = Realm.getDefaultInstance();

                    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                    List<Address> addressLocation = null;

                    try {
                        addressLocation = geocoder.getFromLocation(markerPosition.latitude, markerPosition.longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String city = "City";
                    Pattern pattern = Pattern.compile("locality=(.*?),");
                    Matcher matcher = pattern.matcher(addressLocation.toString());

                    while (matcher.find()) {
                        city = matcher.group(1);
                    }

                    LocationDB location = new LocationDB();
                    RealmResults <LocationDB> resultLog = realm.where(LocationDB.class).findAll();
                    for (LocationDB locationDB : resultLog) {
                        if (city.equals(locationDB.getCity())) {
                            error = 0;
                        }
                    }

                    location.setLat(markerPosition.latitude);
                    location.setLng(markerPosition.longitude);
                    location.setCity(city);
                    location.setId(setUniqueId());

                    if (error == 1) {
                        realm.beginTransaction();
                        realm.insert(location);
                        realm.commitTransaction();

                        Intent intent = new Intent(this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        Toast.makeText(getApplicationContext(), "Marker location saved!", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "This city has already saved!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No markers!", Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.buttonCancel:

                if (markerAdd == true) {
                    marker.remove();
                    markerAdd = false;
                }
                break;
        }
    }

    public long setUniqueId() {
        Number num = realm.where(LocationDB.class).max("id");
        if (num == null) return 1;
        else return ((long) num + 1);
    }

    public GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {

            myLocation = new LatLng(location.getLatitude(), location.getLongitude());

            if (markerAdd == false && markerLocation == false) {
                marker = googleMap.addMarker(new MarkerOptions()
                        .position(myLocation)
                        .draggable(true));
                markerAdd = true;
                markerLocation = true;
                markerPosition = myLocation;
                if (googleMap != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16.0f));
                }
            }
        }
    };

    public GoogleMap.OnMapLongClickListener onMapLongClickListener = new GoogleMap.OnMapLongClickListener() {
        @Override
        public void onMapLongClick(LatLng latLng) {
            markerPosition = latLng;
            if (markerAdd == false) {
                marker = googleMap.addMarker(new MarkerOptions()
                        .position(markerPosition)
                        .draggable(true));
                markerAdd = true;
            } else {
                Toast.makeText(getApplicationContext(), "Marker has already add!", Toast.LENGTH_LONG).show();
            }
        }
    };

    public GoogleMap.OnMapClickListener onMapClickListener = new GoogleMap.OnMapClickListener() {

        @Override
        public void onMapClick(LatLng latLng) {

            if (markerAdd == true) {
                marker.remove();
                markerAdd = false;
            } else {
                Toast.makeText(getApplicationContext(), "No markers!", Toast.LENGTH_LONG).show();
            }
        }
    };

    public GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener = new GoogleMap.OnMyLocationButtonClickListener() {
        @Override
        public boolean onMyLocationButtonClick() {
            if (markerAdd == false && myLocation!= null) {
                marker = googleMap.addMarker(new MarkerOptions()
                        .position(myLocation)
                        .draggable(true));
                markerAdd = true;
                if (googleMap != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16.0f));
                }
            } else {
                Toast.makeText(getApplicationContext(), "Marker has already add or your location is not found!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
    };


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
            googleMap.setOnMapClickListener(onMapClickListener);
            googleMap.setOnMapLongClickListener(onMapLongClickListener);
            googleMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Get your location... Please wait!", Toast.LENGTH_SHORT).show();
                onMapReady(googleMap);
            }
        }
    }

}
