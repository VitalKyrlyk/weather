package testapp.weather;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import testapp.weather.RealmDB.LocationDB;
import testapp.weather.RealmDB.WeatherDB;
import testapp.weather.RecyclerViewAdapters.RVAdapterWeather;
import testapp.weather.Retrofit.WeatherAPI;
import testapp.weather.Retrofit.WeatherDay;

public class Weather extends AppCompatActivity {

    String TAG = "myTag";
    WeatherAPI.ApiInterface api;
    Double lat;
    Double lng;
    Realm realm;
    String city;
    SharedPreferences sPref;
    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather);

        realm = Realm.getDefaultInstance();
        Intent intent = getIntent();

        sPref = getSharedPreferences("cityId", MODE_PRIVATE);
        id = sPref.getLong("ID", 1);

        city = intent.getStringExtra("City");
        lat = intent.getDoubleExtra("Lat", 0.0);
        lng = intent.getDoubleExtra("Lng", 0.0);

        api = WeatherAPI.getClient().create(WeatherAPI.ApiInterface.class);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.weatherList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        RealmResults<WeatherDB> weatherDBRealmResults = realm.where(WeatherDB.class).equalTo("id", id).findAll();

        RealmRecyclerViewAdapter adapter = new RVAdapterWeather(weatherDBRealmResults, true);
        recyclerView.setAdapter(adapter);
        Button checkWeatherButton = (Button) findViewById(R.id.checkWeatherButton);
        checkWeatherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeather(view);
            }
        });
    }

    public long setUniqueId() {
        Number num = realm.where(WeatherDB.class).equalTo("id", id).max("sortedId");
        if (num == null) return 1;
        else return ((long) num + 1);
    }

    public void getWeather(View v) {
        String units = "metric";
        String key = WeatherAPI.KEY;

        Call<WeatherDay> callToday = api.getToday(lat, lng, units, key);
        callToday.enqueue(new Callback<WeatherDay>() {
            @Override
            public void onResponse(@NonNull Call<WeatherDay> call, @NonNull Response<WeatherDay> response) {
                WeatherDay data = response.body();
                Log.d(TAG, response.toString());

                if (response.isSuccessful()) {
                    WeatherDB weatherDB = new WeatherDB();
                    RealmResults<LocationDB> locationDBRealmResults = realm.where(LocationDB.class).findAll();

                    long id;
                    for (LocationDB locationDB : locationDBRealmResults) {
                        if (lat.equals(locationDB.getLat()) && lng.equals(locationDB.getLng())) {

                            id = locationDB.getId();

                            weatherDB.setCity(data.getCity());
                            weatherDB.setTemp(data.getTempWithDegree());
                            weatherDB.setImage(data.getIconUrl());
                            weatherDB.setId(id);
                            weatherDB.setSortedId(setUniqueId());
                        }
                    }
                    realm.beginTransaction();
                    realm.insert(weatherDB);
                    realm.commitTransaction();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherDay> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure");
                Log.e(TAG, t.toString());
            }
        });
    }
}