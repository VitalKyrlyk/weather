package testapp.weather;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import testapp.weather.RealmDB.LocationDB;
import testapp.weather.RecyclerViewAdapters.RVAdapterLocations;

public class MainActivity extends AppCompatActivity {

    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.places);
        Realm.init(this);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.townList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        realm = Realm.getDefaultInstance();
        RealmResults<LocationDB> locationDBRealmResults = realm.where(LocationDB.class).findAll();
        RealmRecyclerViewAdapter adapter = new RVAdapterLocations(locationDBRealmResults, true, getBaseContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setClickable(true);

        Button addPlaceButton = (Button) findViewById(R.id.addPlaceButton);
        addPlaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), Map.class);
                startActivity(intent);
            }
        });

    }

}
