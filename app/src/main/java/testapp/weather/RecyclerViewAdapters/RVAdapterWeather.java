package testapp.weather.RecyclerViewAdapters;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.Sort;
import testapp.weather.R;
import testapp.weather.RealmDB.WeatherDB;

import static android.content.Context.MODE_PRIVATE;


public class RVAdapterWeather extends RealmRecyclerViewAdapter<WeatherDB, RVAdapterWeather.WeatherViewHolder> {

    private Realm realm;
    private OrderedRealmCollection<WeatherDB> weatherDBOrderedRealmCollection;
    private SharedPreferences sPref;
    private long id;
    View v;

    public RVAdapterWeather(@Nullable OrderedRealmCollection<WeatherDB> data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.weatherDBOrderedRealmCollection = data;
    }


    @NonNull
    @Override
    public RVAdapterWeather.WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_weather, null);
        RVAdapterWeather.WeatherViewHolder weatherViewHolder = new RVAdapterWeather.WeatherViewHolder(view);
        realm = Realm.getDefaultInstance();
        sPref = parent.getContext().getSharedPreferences("cityId", MODE_PRIVATE);
        v = parent;
        id = sPref.getLong("ID", 1);
        return weatherViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RVAdapterWeather.WeatherViewHolder holder, final int position) {

        weatherDBOrderedRealmCollection = realm.where(WeatherDB.class).equalTo("id", id).findAllSorted("sortedId", Sort.DESCENDING);
        final WeatherDB weatherDB = weatherDBOrderedRealmCollection.get(position);

        holder.textCity.setText(weatherDB.getCity());
        holder.textTemp.setText(weatherDB.getTemp());
        Uri uri = Uri.parse(weatherDB.getImage());
        Glide.with(v.getContext()).load(uri).into(holder.tvImage);
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return isDataValid() ? weatherDBOrderedRealmCollection.size() : 0;
    }

    static class WeatherViewHolder extends RecyclerView.ViewHolder{
        CardView cvWeather;
        TextView textCity;
        TextView textTemp;
        ImageView tvImage;
        View view;
        WeatherViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            cvWeather = (CardView)itemView.findViewById(R.id.cvWeather);
            textCity = (TextView)itemView.findViewById(R.id.textCity);
            textTemp = (TextView) itemView.findViewById(R.id.textTemp);
            tvImage = (ImageView) itemView.findViewById(R.id.tvImage);
        }
    }

    private boolean isDataValid() {
        return weatherDBOrderedRealmCollection != null && weatherDBOrderedRealmCollection.isValid();
    }
}
