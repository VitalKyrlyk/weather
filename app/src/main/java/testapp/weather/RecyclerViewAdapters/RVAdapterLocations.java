package testapp.weather.RecyclerViewAdapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;
import testapp.weather.R;
import testapp.weather.RealmDB.LocationDB;
import testapp.weather.RealmDB.WeatherDB;
import testapp.weather.Weather;

import static android.content.Context.MODE_PRIVATE;

public class RVAdapterLocations extends RealmRecyclerViewAdapter<LocationDB, RVAdapterLocations.LocationViewHolder> {

    private Realm realm;
    private OrderedRealmCollection<LocationDB> locationDBOrderedRealmCollection;
    private Context context;
    private SharedPreferences sPref;

    private final String SAVED_ID = "ID";

    public RVAdapterLocations(@Nullable OrderedRealmCollection<LocationDB> data, boolean autoUpdate, Context context) {
        super(data, autoUpdate);
        this.locationDBOrderedRealmCollection = data;
        this.context = context;
    }


    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_places, null);
        LocationViewHolder locationViewHolder = new LocationViewHolder(view);
        realm = Realm.getDefaultInstance();
        sPref = parent.getContext().getSharedPreferences("cityId", MODE_PRIVATE);
        return locationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final LocationViewHolder holder, final int position) {

        locationDBOrderedRealmCollection = realm.where(LocationDB.class).findAllSorted("id", Sort.DESCENDING);
        final LocationDB locationDB = locationDBOrderedRealmCollection.get(position);
        holder.textAddress.setText(locationDB.getCity());
        final String coordinate = "Latitude: " + locationDB.getLat() + "; Longitude: " + locationDB.getLng() + ".";
        holder.textCoordinate.setText(coordinate);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor ed = sPref.edit();

                ed.putLong(SAVED_ID, locationDB.getId());
                ed.apply();

                String city =  holder.textAddress.getText().toString();

                Intent intent = new Intent(view.getContext(), Weather.class);
                intent.putExtra("City", city);
                intent.putExtra("Id", locationDB.getId());
                intent.putExtra("Lat", locationDB.getLat());
                intent.putExtra("Lng", locationDB.getLng());
                context.startActivity(intent);
            }
        });
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return isDataValid() ? locationDBOrderedRealmCollection.size() : 0;
    }

    static class LocationViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView textAddress;
        TextView textCoordinate;
        View view;
        LocationViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            cv = (CardView)itemView.findViewById(R.id.cvPlaces);
            textAddress = (TextView)itemView.findViewById(R.id.textAddress);
            textCoordinate = (TextView) itemView.findViewById(R.id.textCoordinate);
        }
    }

    private boolean isDataValid() {
        return locationDBOrderedRealmCollection != null && locationDBOrderedRealmCollection.isValid();
    }
}
